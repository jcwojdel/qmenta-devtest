# qmenta-devtest

At the moment it is a simple API that is served from aiohttp server.

## Install and run

- `git clone` this repository
- `pip install -r requirements.txt` make sure you do it in virtualenv with python3
- `pip install -r requirements-dev.txt` if you want to run tests (`pytest`)
- `python -m server.main` will run a simple server on port 8080
- `python -m aiohttp_devtools runserver server` will run a development, auto reloading version of the server

## What it does?

Check the following URLs:

- http://localhost:8000/date
- http://localhost:8000/dicom
- http://localhost:8000/dicom?filename=T1W_SE/IM-0003-0017.dcm

## Live demo

This code was deployed to Heroku, so you can see it [here](http://qmenta-devtest.herokuapp.com/). Note that by
accident I created it in US region, which means it reacts rather sluggishly when called from Europe. A nice
side-effect is that it makes the reloading visible in the corresponding [frontend](http://qmenta-fe.herokuapp.com/).
Another funny side-effect is that the date received from the API might not be quite what you expect due to timezone differences.


