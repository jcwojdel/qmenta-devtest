from unittest.mock import patch

import pytest
from aiohttp import web

from server.dicom_view import validate_path
from server.main import get_app


async def test_dicom_view_returns_success_status(aiohttp_client):
    client = await aiohttp_client(get_app())
    with patch('server.dicom_view.read_local_dicom') as dcm_mock:
        dcm_mock.return_value.PatientName.original_string = 'testname'
        resp = await client.get('/dicom')
    assert resp.status == 200


def test_validate_path_normalises():
    assert validate_path('directory/../realdirectory/file') == 'realdirectory/file'


def test_validate_path_raises_if_going_up_directory():
    with pytest.raises(web.HTTPBadRequest):
        validate_path('directory/../../realdirectory/file')
