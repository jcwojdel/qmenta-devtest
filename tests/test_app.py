from server import main


async def test_root_handle_status():
    result = await main.handle(None)
    assert result.status == 200


async def test_root_handle_contents():
    result = await main.handle(None)
    assert result.text
