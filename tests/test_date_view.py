import datetime
from unittest.mock import patch

from server.main import get_app


async def test_date_view_returns_success_status(aiohttp_client):
    client = await aiohttp_client(get_app())
    resp = await client.get('/date')
    assert resp.status == 200


async def test_date_view_returns_today(aiohttp_client):
    client = await aiohttp_client(get_app())
    # This mock is a bit silly, but mocking clib datetime is a bit tricky
    with patch('server.date_view.datetime') as datetime_mock:
        datetime_mock.date.today.return_value = datetime.date(2018, 1, 21)
        resp = await client.get('/date')
        result = await resp.json()
    assert result == {'date': '2018-01-21'}
