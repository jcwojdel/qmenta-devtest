import os

from aiohttp import web
import pydicom

DICOM_PATH = 'data/'
DEFAULT_FILENAME = 'T1W_SE/IM-0003-0014.dcm'

async def handle(request):
    filename = request.query.get('filename') or DEFAULT_FILENAME
    filename = validate_path(filename)

    dstream = read_local_dicom(os.path.join(DICOM_PATH, filename))

    return web.json_response({
        'patient_name': dstream.PatientName.original_string,
        'file_name': filename,
    }, headers={'Access-Control-Allow-Origin': '*'})


def read_local_dicom(full_name):
    with open(full_name, 'rb') as f:
        dstream = pydicom.dcmread(f, stop_before_pixels=True)
    return dstream


def validate_path(filepath):
    normpath = os.path.normpath(filepath)
    if normpath.startswith('..'):
        raise web.HTTPBadRequest(text='Only paths inside data directory are allowed')
    return normpath
