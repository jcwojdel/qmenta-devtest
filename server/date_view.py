import datetime

from aiohttp import web


DATE_FORMAT = '%Y-%m-%d'


async def handle(_request):
    return web.json_response({
        'date': datetime.date.today().strftime(DATE_FORMAT),
    }, headers={'Access-Control-Allow-Origin': '*'})
