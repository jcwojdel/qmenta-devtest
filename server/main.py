from aiohttp import web
from server.date_view import handle as date_handle
from server.dicom_view import handle as dicom_handle


async def handle(request):
    return web.Response(text='Qmenta dev test server is up and running')


def get_app():
    app = web.Application()
    app.add_routes([
        web.get('/', handle),
        web.get('/date', date_handle),
        web.get('/dicom', dicom_handle),
    ])
    return app

app = get_app()


if __name__ == '__main__':
    web.run_app(app)
